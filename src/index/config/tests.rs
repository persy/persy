use crate::util::io::ArcSliceRead;

use crate::index::config::{IndexConfig, ValueMode};

#[test]
fn test_config_ser_des() {
    let cfg = IndexConfig {
        name: "abc".to_string(),
        root: None,
        key_type: 1,
        value_type: 1,
        page_min: 10,
        page_max: 30,
        value_mode: ValueMode::Replace,
    };

    let mut buff = Vec::new();
    cfg.serialize(&mut buff);
    let read = IndexConfig::deserialize(&mut ArcSliceRead::new_vec(buff)).expect("deserialization works");
    assert_eq!(cfg, read);
}
