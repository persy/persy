use crate::{error::GenericError, persy::PersyImpl, IndexOpsError, IndexType, Persy, PersyId, PE};
use std::sync::Arc;

mod inspect_tree;
#[cfg(test)]
mod tests;

pub type TreeInspectorResult<T> = std::result::Result<T, GenericError>;

pub(crate) use inspect_tree::inspect_tree;
pub use inspect_tree::PrintTreeInspector;

pub trait TreeInspector<K, V> {
    fn start_node(&mut self, _node_id: PersyId, _prev: Option<K>, _next: Option<K>) -> TreeInspectorResult<()> {
        Ok(())
    }
    fn failed_load(&mut self, _node_id: PersyId) -> TreeInspectorResult<()> {
        Ok(())
    }
    fn end_node(&mut self, _node_id: PersyId) -> TreeInspectorResult<()> {
        Ok(())
    }
    fn start_leaf(&mut self, _node_id: PersyId, _prev: Option<K>, _next: Option<K>) -> TreeInspectorResult<()> {
        Ok(())
    }
    fn end_leaf(&mut self, _node_id: PersyId) -> TreeInspectorResult<()> {
        Ok(())
    }
    fn start_key(&mut self, _node_pos: u32, _k: K) -> TreeInspectorResult<()> {
        Ok(())
    }
    fn end_key(&mut self, _node_pos: u32, _k: K) -> TreeInspectorResult<()> {
        Ok(())
    }
    fn value(&mut self, _node_pos: u32, _v: V) -> TreeInspectorResult<()> {
        Ok(())
    }
    fn empty(&mut self) -> TreeInspectorResult<()> {
        Ok(())
    }
}

pub trait PersyInspect {
    fn inspect_tree<K, V, I>(&self, index_name: &str, inspector: &mut I) -> Result<(), PE<IndexOpsError>>
    where
        K: IndexType,
        V: IndexType,
        I: TreeInspector<K, V>;

    fn page_state_scan(&self) -> Result<PageStateIter, PE<GenericError>>;
}

pub struct PageState {
    position: u64,
    exp: u8,
    free: bool,
}

impl PageState {
    pub(crate) fn new(position: u64, exp: u8, free: bool) -> Self {
        Self { position, exp, free }
    }
    pub fn position(&self) -> u64 {
        self.position
    }
    pub fn size(&self) -> u64 {
        1 << self.exp
    }
    pub fn size_exp(&self) -> u8 {
        self.exp
    }
    pub fn is_free(&self) -> bool {
        self.free
    }
}

pub struct PageStateIter {
    persy_impl: Arc<PersyImpl>,
    next: u64,
}
impl PageStateIter {
    fn new(persy_impl: Arc<PersyImpl>) -> Self {
        Self { persy_impl, next: 0 }
    }
}
impl Iterator for PageStateIter {
    type Item = PageState;
    fn next(&mut self) -> Option<Self::Item> {
        let next = if self.next == 0 {
            Some(PageState::new(self.next, crate::persy::DEFAULT_PAGE_EXP, false))
        } else {
            self.persy_impl.page_state(self.next)
        };
        if let Some(n) = &next {
            self.next = n.position() + n.size();
        }
        next
    }
}

impl PersyInspect for Persy {
    fn inspect_tree<K, V, I>(&self, index_name: &str, inspector: &mut I) -> Result<(), PE<IndexOpsError>>
    where
        K: IndexType,
        V: IndexType,
        I: TreeInspector<K, V>,
    {
        let index_id = self
            .solve_index_id(index_name)
            .map_err(|e| PE::PE(IndexOpsError::from(e.error())))?;
        self.persy_impl
            .inspect_tree::<K::Wrapper, V::Wrapper, I>(index_id, inspector)?;
        Ok(())
    }

    fn page_state_scan(&self) -> Result<PageStateIter, PE<GenericError>> {
        Ok(PageStateIter::new(self.persy_impl.clone()))
    }
}
