use crate::{
    inspect::{PersyInspect, TreeInspector},
    OpenOptions, ValueMode,
};

#[derive(Default)]
struct Count {
    keys_count: u32,
    values_count: u32,
    enabled: bool,
}
impl<K, V> TreeInspector<K, V> for Count {
    fn start_node(
        &mut self,
        _node_id: crate::PersyId,
        _prev: Option<K>,
        _next: Option<K>,
    ) -> super::TreeInspectorResult<()> {
        Ok(())
    }

    fn end_node(&mut self, _node_id: crate::PersyId) -> super::TreeInspectorResult<()> {
        Ok(())
    }

    fn start_leaf(
        &mut self,
        _node_id: crate::PersyId,
        _prev: Option<K>,
        _next: Option<K>,
    ) -> super::TreeInspectorResult<()> {
        self.enabled = true;
        Ok(())
    }

    fn end_leaf(&mut self, _node_id: crate::PersyId) -> super::TreeInspectorResult<()> {
        self.enabled = false;
        Ok(())
    }

    fn start_key(&mut self, _node_pos: u32, _k: K) -> super::TreeInspectorResult<()> {
        if self.enabled {
            self.keys_count += 1;
        }
        Ok(())
    }

    fn end_key(&mut self, _node_pos: u32, _k: K) -> super::TreeInspectorResult<()> {
        Ok(())
    }

    fn value(&mut self, _node_pos: u32, _v: V) -> super::TreeInspectorResult<()> {
        self.values_count += 1;
        Ok(())
    }

    fn failed_load(&mut self, _node_id: crate::PersyId) -> super::TreeInspectorResult<()> {
        Ok(())
    }

    fn empty(&mut self) -> super::TreeInspectorResult<()> {
        Ok(())
    }
}

#[test]
pub fn test_tree_inspection() {
    let p = OpenOptions::new().memory().unwrap();
    let mut tx = p.begin().unwrap();
    tx.create_index::<u32, u32>("index", ValueMode::Cluster).unwrap();
    tx.prepare().unwrap().commit().unwrap();

    let mut tx = p.begin().unwrap();
    for x in 0..20000 {
        for z in 0..20 {
            tx.put::<u32, u32>("index", x, x * 100 + z).unwrap();
        }
    }
    tx.prepare().unwrap().commit().unwrap();
    let mut ti = Count::default();

    p.inspect_tree::<u32, u32, _>("index", &mut ti).unwrap();

    assert_eq!(ti.keys_count, 20000);
    assert_eq!(ti.values_count, 20000 * 20);
}

#[derive(Default)]
struct Value {
    key: Option<u32>,
    value: Option<u32>,
}
impl TreeInspector<u32, u32> for Value {
    fn start_node(
        &mut self,
        _node_id: crate::PersyId,
        _prev: Option<u32>,
        _next: Option<u32>,
    ) -> super::TreeInspectorResult<()> {
        Ok(())
    }

    fn end_node(&mut self, _node_id: crate::PersyId) -> super::TreeInspectorResult<()> {
        Ok(())
    }

    fn start_leaf(
        &mut self,
        _node_id: crate::PersyId,
        _prev: Option<u32>,
        _next: Option<u32>,
    ) -> super::TreeInspectorResult<()> {
        Ok(())
    }

    fn end_leaf(&mut self, _node_id: crate::PersyId) -> super::TreeInspectorResult<()> {
        Ok(())
    }

    fn start_key(&mut self, _node_pos: u32, k: u32) -> super::TreeInspectorResult<()> {
        self.key = Some(k);
        Ok(())
    }

    fn end_key(&mut self, _node_pos: u32, _k: u32) -> super::TreeInspectorResult<()> {
        Ok(())
    }

    fn value(&mut self, _node_pos: u32, v: u32) -> super::TreeInspectorResult<()> {
        self.value = Some(v);
        Ok(())
    }

    fn failed_load(&mut self, _node_id: crate::PersyId) -> super::TreeInspectorResult<()> {
        Ok(())
    }

    fn empty(&mut self) -> super::TreeInspectorResult<()> {
        Ok(())
    }
}

#[test]
pub fn test_tree_value_inspection() {
    let p = OpenOptions::new().memory().unwrap();
    let mut tx = p.begin().unwrap();
    tx.create_index::<u32, u32>("index", ValueMode::Cluster).unwrap();
    tx.prepare().unwrap().commit().unwrap();

    let mut tx = p.begin().unwrap();
    tx.put::<u32, u32>("index", 100, 200).unwrap();
    tx.prepare().unwrap().commit().unwrap();
    let mut ti = Value::default();

    p.inspect_tree::<u32, u32, _>("index", &mut ti).unwrap();

    assert_eq!(ti.key, Some(100));
    assert_eq!(ti.value, Some(200));
}

#[test]
pub fn test_page_scan() {
    let p = OpenOptions::new().memory().unwrap();
    let mut tx = p.begin().unwrap();
    tx.create_segment("test").unwrap();
    tx.prepare().unwrap().commit().unwrap();

    let mut tx = p.begin().unwrap();
    tx.insert("test", &vec![10, 10, 10]).unwrap();
    tx.prepare().unwrap().commit().unwrap();

    assert_eq!(p.page_state_scan().unwrap().count(), 9);
}
