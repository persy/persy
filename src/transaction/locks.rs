use crate::id::{RecRef, SegmentId};

#[derive(Clone)]
pub(crate) struct Locks {
    records: Vec<RecRef>,
    created_segments: Vec<String>,
    created_updated_segments: Vec<SegmentId>,
    dropped_segments: Vec<SegmentId>,
}

impl Locks {
    pub(super) fn new(
        records: Vec<RecRef>,
        created_segments: Vec<String>,
        created_updated_segments: Vec<SegmentId>,
        dropped_segments: Vec<SegmentId>,
    ) -> Self {
        Self {
            records,
            created_segments,
            created_updated_segments,
            dropped_segments,
        }
    }

    pub(crate) fn add_records<S: Iterator<Item = RecRef>>(&mut self, s: S) {
        self.records.extend(s);
    }

    pub(crate) fn add_create_update_segments<S: Iterator<Item = SegmentId>>(&mut self, s: S) {
        self.created_updated_segments.extend(s);
    }
    pub(crate) fn records(&self) -> &[RecRef] {
        &self.records
    }
    pub(crate) fn created_segments(&self) -> &[String] {
        &self.created_segments
    }

    pub(crate) fn created_segments_cloned(&self) -> Vec<String> {
        self.created_segments.clone()
    }
    pub(crate) fn created_updated_segments(&self) -> &[SegmentId] {
        &self.created_updated_segments
    }

    pub(crate) fn created_updated_segments_cloned(&self) -> Vec<SegmentId> {
        self.created_updated_segments.clone()
    }
    pub(crate) fn dropped_segments(&self) -> &[SegmentId] {
        &self.dropped_segments
    }
}
