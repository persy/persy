use std::vec::Vec;

#[allow(unused)]
pub trait VecSet<T> {
    fn insert(&mut self, value: T) -> bool;
    fn remove(&mut self, value: &T) -> bool;
    fn contains(&self, value: &T) -> bool;
}

impl<T: Ord> VecSet<T> for Vec<T> {
    fn insert(&mut self, value: T) -> bool {
        match self.binary_search(&value) {
            Ok(_) => false,
            Err(index) => {
                Vec::insert(self, index, value);
                true
            }
        }
    }

    fn remove(&mut self, value: &T) -> bool {
        match self.binary_search(value) {
            Ok(index) => {
                Vec::remove(self, index);
                true
            }
            Err(_) => false,
        }
    }

    fn contains(&self, value: &T) -> bool {
        self.binary_search(value).is_ok()
    }
}
